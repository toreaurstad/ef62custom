﻿#pragma warning disable 1591
namespace System.Data.Entity.Core.Objects
{
    using System.Data.Entity.Core.Metadata.Edm;

    /// <summary>
    /// Event args for Metadata added event
    /// </summary>
    public class MetadataAddingEventArgs : EventArgs 
    {
        /// <summary>
        /// The type of edm
        /// </summary>
        public string EdmType { get; set; }

        /// <summary>
        /// The set of entity set
        /// </summary>
        public string EntitySet { get; set; }

        /// <summary>
        /// The entity set
        /// </summary>
        public string EntityType { get; set; }

    }
}
